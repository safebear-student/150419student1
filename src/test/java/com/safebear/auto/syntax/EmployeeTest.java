package com.safebear.auto.syntax;

import org.testng.annotations.Test;

public class EmployeeTest {

    @Test
    public void testEmployee() {
        //This is where we create our objects
        Employee hannah = new Employee();
        Employee bob = new Employee();
        Employee lol = new Employee();
        //Create a sales employee object
        SalesEmployee victoria = new SalesEmployee();

        //This is where we employ hannah and fire bob
        hannah.setEmployed(false);
        bob.fire();
        lol.setEmployed(true);
        lol.givePayRise(20000);

        //this is where we employ victoria and give her a car
        victoria.setEmployed(true);
        victoria.changeCar("Porsche");

        //Let's print their state to screen
        System.out.println("Hannah employment state:" + hannah.isEmployed());
        System.out.println("Bob employment state:" + bob.isEmployed());
        System.out.println("Lol employment state:" + lol.isEmployed());
        System.out.println("Lol salary:" + lol.getSalary());
        System.out.println("Victoria's employment state:" + victoria.isEmployed());
        System.out.println("Victoria's car:" + victoria.car);

    }
}
