package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class ToolsPage {

    ToolsPageLocators locators = new ToolsPageLocators();

    @Getter
    private String expectedPageTitle = "Tools Page";
    @Getter
    private String expectedLoginSuccessfulMessage = "Login Successful";

    @NonNull
    WebDriver driver;

    public String getPageTitle() {
        return driver.getTitle();

    }

    public String getLoginSuccessfulMessage() {
        return driver.findElement(locators.getSuccessfulLoginMessageLocator()).getText();
    }

    public void deleteJMeterButton() {
        driver.findElement(locators.getJmeterDeleteButton()).click();
    }


}
