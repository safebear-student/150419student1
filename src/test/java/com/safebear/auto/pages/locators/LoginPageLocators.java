package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data

public class LoginPageLocators {

    //fields
private By usernameLocator = By.id("username");
private By passwordFieldLocator = By.name("psw");

//buttons
private By submitButtonLocator = By.xpath(".//button[@id='enter']");

//messages
private By validationWarningMessageLocator = By.xpath(".//p[@id='rejectLogin']/b");

//checkboxes
private By remembermeBoxLocator = By.xpath(".//label/input[@name='remember']");



}

