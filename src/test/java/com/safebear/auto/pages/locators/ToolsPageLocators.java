package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data

public class ToolsPageLocators {

    //messages
    private By successfulLoginMessageLocator = By.xpath(".//body/div[@class='container']/p/b");
    //    private By welcomeTesterMessageLocator = By.xpath(".//h1");
    //Search button
        private By searchButton = By.xpath("//button[@class='btn btn-info']");

        //Delete jmeter button
    private By jmeterDeleteButton = By.xpath("//td[.='JMeter']/following-sibling::td[3]/a[@id='remove']");




}
