package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {

    LoginPageLocators locators = new LoginPageLocators();

    @Getter
    private String expectedPageTitle ="Login Page";

    @NonNull
    WebDriver driver;

    public String getPageTitle() {
        return driver.getTitle();
    }

    public void enterUsername(String username) {
        driver.findElement(locators.getUsernameLocator()).sendKeys(username);
    }

    public void enterPassword(String password) {
        driver.findElement(locators.getPasswordFieldLocator()).sendKeys(password);
    }

    public void clickLoginButton() {
        driver.findElement(locators.getSubmitButtonLocator()).click();
    }

    public void clickRemembermeBox() {
        driver.findElement(locators.getRemembermeBoxLocator()).click();
        driver.findElement(locators.getRemembermeBoxLocator()).isSelected();
    }


    public void login(String username, String password) {
        enterUsername(username);
        enterPassword(password);
        clickLoginButton();
    }

    public String validationWarningMessage (){
        return driver.findElement(locators.getValidationWarningMessageLocator()).getText();
    }
}
