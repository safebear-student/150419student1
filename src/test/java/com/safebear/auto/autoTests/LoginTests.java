package com.safebear.auto.autoTests;

import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;


public class LoginTests extends BaseTest {

    @Test
    public void loginAsValidUserAndCheckMessageTest(){
        driver.get(Utils.getUrl());
        Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle());
        loginPage.login("tester", "letmein");
        Assert.assertEquals(toolsPage.getPageTitle(), toolsPage.getExpectedPageTitle());
        Assert.assertEquals(toolsPage.getLoginSuccessfulMessage(),toolsPage.getExpectedLoginSuccessfulMessage());
    }

    public void loginAsinvalidUserAndCheckMessageTest() {
        driver.get(Utils.getUrl());
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedPageTitle());
        loginPage.login("attack", "dontletmein");
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedPageTitle());
    }


}
