package com.safebear.auto.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.GeckoDriverInfo;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

public class Utils {

//    private static final String URL = System.getProperty("url", "http://toolslist.safebear.co.uk:8080");
    private static final String URL = System.getProperty("url", "http://localhost:8080");

    private static final String BROWSER = System.getProperty("browser", "chrome");

    public static String getUrl() {
        return URL;
    }

    public static WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/driver/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver","src/test/resources/driver/geckodriver64.exe" );
        System.setProperty("webdriver.ie.driver", "src/test/resources/driver/Windows8.1-KB2990999-x64.msu");
        ChromeOptions options = new ChromeOptions();
        FirefoxOptions options1 = new FirefoxOptions();
        InternetExplorerOptions options2 = new InternetExplorerOptions();
        options.addArguments("window-size=1366,768");

        switch (BROWSER) {
            case "chrome":
                return new ChromeDriver(options);

            case "firefox":
                return new FirefoxDriver(options1);

            case "IE":
                return new InternetExplorerDriver(options2);

            case "chromeheadless":
                options.addArguments("chromeheadless", "disable-gpu");
                return new ChromeDriver(options);

            case "firefoxheadless":
                options1.addArguments("--headless");
                return new FirefoxDriver(options1);

            default:
                return new ChromeDriver(options);
        }
    }
}
